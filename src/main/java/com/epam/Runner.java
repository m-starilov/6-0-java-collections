package com.epam;

import com.epam.flat.Flat;
import com.epam.flat.appliance.HomeAppliance;
import com.epam.flat.appliance.specifications.Type;
import java.util.Arrays;
import java.util.List;

public class Runner {
  static List<HomeAppliance> homeAppliances = Arrays.asList(
    new HomeAppliance("TV", Type.CONSUMER_ELECTRONICS, "LG", "STM-900", 200, false),
    new HomeAppliance("Laptop", Type.CONSUMER_ELECTRONICS, "Asus", "G14-IVA", 180, false),
    new HomeAppliance("Lamp", Type.SMALL_APPLIANCE, "Xiaomi", "light", 20, false),
    new HomeAppliance("Refrigerator", Type.MAJOR_APPLIANCE, "LG", "REF60", 500, false),
    new HomeAppliance("Oven", Type.MAJOR_APPLIANCE, "Bosch", "SHBJ558YS0Q", 3000, false),
    new HomeAppliance("Washing machine", Type.MAJOR_APPLIANCE, "LG", "FHT1265ZNW", 1800, false),
    new HomeAppliance("Iron", Type.SMALL_APPLIANCE, "Tefal", "SV6040 Fasteo Steam", 1500, false),
    new HomeAppliance("Toaster", Type.SMALL_APPLIANCE, "Zepter", "TF-995", 1500, false),
    new HomeAppliance("TV", Type.CONSUMER_ELECTRONICS, "Philips", "STM-900", 180, false),
    new HomeAppliance("Lamp", Type.SMALL_APPLIANCE, "Xiaomi", "light2", 25, false)
  );

  public static void main(String[] args){
    Flat flat = new Flat(homeAppliances);
    System.out.println("Список домашних электроприборов: " + flat);

    flat.getHomeAppliances().get(0).plugInAppliance();
    flat.getHomeAppliances().get(2).plugInAppliance();
    flat.getHomeAppliances().get(3).plugInAppliance();
    flat.getHomeAppliances().get(7).plugInAppliance();
    System.out.println("Некоторые электроприборы были включены в сеть " + flat);

    System.out.println("Сумма мощности включеных приборов - " +
        flat.calculatePowerConsumption() + " Ватт");

    flat.sortByPower();
    System.out.println("Список домашних электроприборов отсортирован по мощности " + flat);

    System.out.println("C параметрами name - \"TV\" и model - \"STM-900\" найден электроприбор -> ");
    System.out.println(flat.findHomeAppliance("TV", "STM-900"));

    System.out.println("C параметрами \"Lamp\", \"SMALL_APPLIANCE\", \"Xiaomi\", \"light\", \"20\" "
        + "найден электроприбор -> ");
    System.out.println(flat.findHomeAppliance(
        "Lamp",
        Type.SMALL_APPLIANCE,
        "Xiaomi",
        "light",
        20));

    System.out.println("C параметрами \"Toaster\", \"SMALL_APPLIANCE\", \"Zepter\", \"TF-995\", "
        + "\"1500\", plug in - true найден электроприбор -> ");
    System.out.println(flat.findHomeAppliance(
        "Toaster",
        Type.SMALL_APPLIANCE,
        "Zepter",
        "TF-995",
        1500,
        true));
  }
}

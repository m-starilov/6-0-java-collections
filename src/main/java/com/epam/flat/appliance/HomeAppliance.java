package com.epam.flat.appliance;

import com.epam.flat.appliance.specifications.Type;
import java.util.Objects;

public class HomeAppliance {
  private final String name;
  private final Type type;
  private final String manufacturer;
  private final String model;
  private final int powerInWatt;
  private boolean plugIn;

  public HomeAppliance(String name, Type type, String manufacturer, String model, int powerInWatt,
      boolean plugIn) {
    this.name = name;
    this.type = type;
    this.manufacturer = manufacturer;
    this.model = model;
    this.powerInWatt = powerInWatt;
    this.plugIn = plugIn;
  }

  public String getName() {
    return name;
  }

  public Type getType() {
    return type;
  }

  public String getManufacturer() {
    return manufacturer;
  }

  public String getModel() {
    return model;
  }

  public int getPowerInWatt() {
    return powerInWatt;
  }

  public boolean isPlugIn() {
    return plugIn;
  }

  public void setPlugIn(boolean plugIn) {
    this.plugIn = plugIn;
  }

  public void plugInAppliance() {
    if (!isPlugIn()) setPlugIn(true);
  }

  @Override
  public String toString() {
    return "\n HomeAppliance{" +
        "name='" + name + '\'' +
        ", type=" + type +
        ", manufacturer='" + manufacturer + '\'' +
        ", model='" + model + '\'' +
        ", powerInWatt=" + powerInWatt +
        ", plugIn=" + plugIn +
        '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HomeAppliance that = (HomeAppliance) o;
    return powerInWatt == that.powerInWatt && plugIn == that.plugIn && name.equals(that.name)
        && type == that.type && manufacturer.equals(that.manufacturer) && model.equals(that.model);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, type, manufacturer, model, powerInWatt, plugIn);
  }
}

package com.epam.flat.appliance.specifications;

public enum Type { MAJOR_APPLIANCE, SMALL_APPLIANCE, CONSUMER_ELECTRONICS }

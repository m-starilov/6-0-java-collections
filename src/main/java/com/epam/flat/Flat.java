package com.epam.flat;

import com.epam.flat.appliance.HomeAppliance;
import com.epam.flat.appliance.specifications.Type;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Flat {
  private final List<HomeAppliance> homeAppliances;

  public Flat(List<HomeAppliance> homeApplianceList) {
    this.homeAppliances = homeApplianceList;
  }

  public List<HomeAppliance> getHomeAppliances() {
    return homeAppliances;
  }

  public void sortByPower() {
    homeAppliances.sort(Comparator.comparingInt(HomeAppliance::getPowerInWatt));
  }

  public int calculatePowerConsumption() {
    return homeAppliances.stream().filter(HomeAppliance::isPlugIn)
        .mapToInt(HomeAppliance::getPowerInWatt).sum();
  }

  public List<HomeAppliance> findHomeAppliance(String name, String model) {
    return homeAppliances.stream().filter(appliance -> Objects.equals(appliance.getName(), name))
        .filter(appliance -> Objects.equals(appliance.getModel(), model))
        .collect(Collectors.toList());
  }

  public List<HomeAppliance> findHomeAppliance(String name, Type type, String manufacturer, String model,
      int powerInWatt) {
    return findHomeAppliance(name, model).stream()
        .filter(appliance -> Objects.equals(appliance.getType(), type))
        .filter(appliance -> Objects.equals(appliance.getManufacturer(), manufacturer))
        .filter(appliance -> Objects.equals(appliance.getPowerInWatt(), powerInWatt))
        .collect(Collectors.toList());
  }

  public List<HomeAppliance> findHomeAppliance(String name, Type type, String manufacturer, String model,
      int powerInWatt, boolean plugIn) {
    return findHomeAppliance(name, type, manufacturer, model, powerInWatt).stream()
        .filter(appliance -> plugIn).collect(Collectors.toList());
  }

  @Override
  public String toString() {
    return "Flat{" +
        "homeAppliances=" + homeAppliances +
        '}';
  }
}
